<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Christoph Stach - Page');

Tx_Flux_Core::registerProviderExtensionKey('christoph_stach_page', 'Page');